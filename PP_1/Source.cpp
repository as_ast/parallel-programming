#include <iostream>
#include <process.h>
#include <Windows.h>

const int COUNT = 500;
const int NTHREAD = 4;

struct INFORM {
	int* a;
	int left, right;
	int max = 0;
	bool hasFound = false;
};

int max_nonparallel(int* arr) {
	int max = arr[0];
	for (size_t i = 1; i < COUNT; ++i) {
		if (arr[i] % 2 == 0)
			continue;
		max = std::max<int>(max, arr[i]);
	}
	return max;
}

unsigned __stdcall maximum(void* arg) {
	INFORM* inform = (INFORM*)arg;

	inform->max = inform->a[0];
	for (int i = inform->left; i < inform->right; ++i) {
		if (inform->a[i] % 2 == 0)
			continue;
		inform->max = std::max<int>(inform->max, inform->a[i]);
	}

	_endthreadex(0);
	if (inform->max % 2 == 0)
		inform->hasFound = false;
	else
		inform->hasFound = true;
	return(0);
}

int max_parallel(int* arr) {
	HANDLE t[NTHREAD];
	unsigned ThreadID[NTHREAD];
	INFORM informs[NTHREAD];
	int block_size = COUNT / NTHREAD;
	for (int i = 0; i < NTHREAD; i++) {
		informs[i].a = arr;
		informs[i].left = block_size * i;
		informs[i].max = 0;
		if (i == NTHREAD - 1) {
			informs[i].right = COUNT;
		}
		else {
			informs[i].right = block_size * (i + 1);
		}

		t[i] = (HANDLE)_beginthreadex(nullptr, 0, &maximum, &informs[i], 0, &ThreadID[i]);
	}

	WaitForMultipleObjects(NTHREAD, t, true, INFINITE);

	int global_max = informs[0].max;
	for (int i = 1; i < NTHREAD; i++) {
		if (!informs[i].hasFound)
			continue;
		global_max = std::max<int>(informs[i].max, global_max);
	}

	for (int i = 0; i < NTHREAD; i++) {
		CloseHandle(t[i]);
	}

	return global_max;
}



int main() {
	int test_arr[COUNT];
	for (int i = 0; i < COUNT; i++) {
		//test_arr[i] = (rand() % 100) * 2;
		test_arr[i] = (rand() % 100);
	}

	int max_np = max_nonparallel(test_arr);
	int max_p = max_parallel(test_arr);

	if (max_p % 2 == 0) {
		std::cout << "There is no nechetniye\n";
		return 0;
	}
	std::cout << "Non parallel: " << max_np << "\nParallel: " << max_p << std::endl;

	return 0;
}

